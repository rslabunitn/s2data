function [ perc ] = percentile( obj, varargin )
%PERCENTILE Summary of this function goes here
%   Detailed explanation goes here
%     nargin

    

    p_inf = 0;
    p_max = 100;
    p=inputParser;
    addParameter(p,'percs',[p_inf p_max]);
    parse(p,varargin{:});
    perc=zeros(size(obj.image,3),2);
    for i = 1:size(obj.image,3)       
        perc(i,1)  = min(prctile(obj.image(:,:,i), p.Results.percs(1,1)));
        perc(i,2)  = max(prctile(obj.image(:,:,i), p.Results.percs(1,2))); 
    end
    
    
    
    
%     perc=zeros(size(obj.data,1),2);
%     for i = 1:size(obj.data,1)       
%         perc(i,1)  = min(prctile(obj.data{i}, p.Results.percs(1,1)));
%         perc(i,2)  = max(prctile(obj.data{i}, p.Results.percs(1,2))); 
%     end
      %  obj.data{i} = min(xmax,obj.data{i});     
end

