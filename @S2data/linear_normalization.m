function [ out, obj ] = linear_normalization(obj, I, varargin)
%LINEARNORMALIZATION normalize the input image between 0 and 1
%   by default the image is normalized between min and max
%	if 'Limits' parameter is passed that values are used
%	
%	Example:
%			normImage = linearNormalization(image, 'Limits',  [100 1e4])

%% Preprocessing of inputs

% find the max and min for each band

maxForEachBand =  squeeze(max(max(I)));
minForEachBand =  squeeze(min(min(I)));



%start a new parser
p = inputParser;

% Define Name of the parameter and default value 
addParameter(p,'Limits', [minForEachBand maxForEachBand] ); %Output of function "percentile.m" can be used
addParameter(p,'load_image', 0);

% parse the inputs
parse(p,varargin{:});

% extract the max and min for each band
minVal = p.Results.Limits(:,1);
maxVal = p.Results.Limits(:,2);

% set min and max of each band to [minVal maxVal] if declared
if(size(minVal,1)==1)
    if (size(I,3)>1)
      minVal= ones(size(I,3),1).*minVal; 
      maxVal= ones(size(I,3),1).*maxVal;
    end
end


%% Main Code

% initialize the output image
out = zeros(size(I));


for i=1:size(I,3)
	out(:,:,i) = (I(:,:,i) - minVal(i))/(maxVal(i) - minVal(i));
end

% clip the output image between 0 and 1
out = max(0,out);
out = min(1,out);

if p.Results.load_image==1
    obj.image=out;
    obj.limit=[minVal maxVal];
end

end

