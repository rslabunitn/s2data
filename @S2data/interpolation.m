function [ obj ] = interpolation( obj, varargin )
% The function takes an image and interpolates. The output image has
% the number of rows and columns equal to the maximum number of rows of
% the biggest cell array. The image is already normalized and the noise
% is removed (quantile 0.98).
%
% INPUTS
%   I:      input image  
% 
% OUTPUTS
%   O:      interpolated image
% 
% Example:
%			inter = interpolation(image)
%
% Matteo Soave e Giulio Weikmann 
% matteo.soave@studenti.unitn.it  giulio.weikmann@studenti.unitn.it 
   
%--------------------------------------------------------------------------    
% Interpolate data
%--------------------------------------------------------------------------
    method=[];
    p=inputParser;
    addParameter(p,'Method',method);
    parse(p,varargin{:});

    xmax=0;
    
%     for i = 1:size(obj.data,1)       
%         xmax  = max(quantile(obj.data{i,1}, 0.98));
%         obj.data{i,1} = min(xmax,obj.data{i,1});                   
%     end

    
% Search the largest image (rows)

    a=1;
    for i = 1:size(obj.data,1)
        temp = max(size(obj.data{i}));
        if (xmax < temp) 
            a = i;
        end
        xmax = max(xmax,temp);
    end

    
    im_int = zeros([size(obj.data{a}),size(obj.data,1)]);
    
% Resize
 
    if(isempty(p.Results.Method))
        for i = 1:size(obj.data,1)   
            im_int(:,:,i) = imresize(obj.data{i},[size(obj.data{a},1) size(obj.data{a},2)]);
        end
    else
        for i = 1:size(obj.data,1)   
            im_int(:,:,i) = imresize(obj.data{i},size(obj.data{a},1)/size(obj.data{i},1),'nearest');
        end
    end
     
     obj.image=im_int;
     
end
 
