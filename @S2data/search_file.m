function [ path ] = search_file(obj, original_path, pattern, ext )
%SEARCH_FILE Summary of this function goes here
%   Detailed explanation goes here
    path=[];
    list=dir(original_path);
    list=list(3:end);
    i=1;
    if isempty(list)
        return;
    end
    while(isempty(path))
        if and(contains(list(i).name,pattern),contains(list(i).name,ext)) 
            path=[original_path '/' list(i).name];
        end
        if list(i).isdir
            path=obj.search_file(obj,[original_path '/' list(i).name],pattern,ext);
        end
        i=i+1;
        if i>size(list,1) return;
        end
    end
end

