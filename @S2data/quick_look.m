function [ out ] = quick_look( obj, bands )
%QUICK_LOOK Summary of this function goes here
%   Detailed explanation goes here
    
    if size(bands,2)>3
        msg = ('Limit of 3 bands. R G B order. ');
        error(msg);
    end


    upd_bands=cell(1,3);    
    for i=1:3
        [a, b] = regexp(bands{1,i},'\d[\d A]');
        if isempty(a)
            [a, b] = regexp(bands{1,i}, '\d');
            upd_bands{i}=['B0' bands{1,i}(a)];
        else
            upd_bands{i} =['B' bands{1,i}(a:b)];
        end
    end
    
    perc=obj.percentile('percs',[0 98]);
        
    for i=1:3
        for j=1:size(obj.bands,2)
            if strcmp(upd_bands{i},obj.bands{j})
                quick_im(:,:,i)=obj.image(:,:,j);
                perc_quick(i,1)=perc(j,1);
                perc_quick(i,2)=perc(j,2);
            end
        end
    end
    
    if size(quick_im,3)<3
        msg = ('Band Missing. ');
        error(msg);
    end

    
%    figure(1),
%    imshow(quick_im(:,:,[1 2 3])/3000);
    
    quick_im = obj.linear_normalization(quick_im,'Limits',perc_quick);
    
%     figure(2),
%     imshow(quick_im(:,:,[1 2 3]));
    
    quick_im=obj.gamma_correction('Gamma',0.8,'image',quick_im);
    
%     figure(3),
    %imshow(quick_im(:,:,[1 2 3]));
    out=quick_im;
    
end


