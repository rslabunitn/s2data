function [ TAB, label ] = shape_read( obj, name, field )
    
    % Class input must be a string containing the name of the label you
    % want to get.

    shape_data = shaperead(name);
    
    TAB=[];
    label=[];
    
    for i=1:size(shape_data,1)
        [row,col]=map2pix(obj.georeference_cut,shape_data(i).X,shape_data(i).Y);
        row=round(row);
        col=round(col);
        row(isnan(row))=[];
        col(isnan(col))=[];
        mat=roipoly(obj.image,col,row);
        [row,col]=find(mat==1);
        pol_set=[row.*ones(size(row,1),1) col.*ones(size(col,1),1)];

        for j=1:size(obj.image,3)
            lin_index=sub2ind(size(obj.image),row, col, j*ones(size(row)));
            pol_set=[pol_set obj.image(lin_index)];
        end   
        TAB=[TAB; pol_set];
        label=[label; shape_data(i).(field)*ones(size(row,1),1)];
    end
    
    
end

