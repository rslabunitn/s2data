function [ classMap ] = svm_classification( obj, model, bands, limits, varargin )
    name=[];
    color_map=[];
    tif=false;
	cprob = false;
    %Parse input
    p=inputParser;
    addParameter(p,'ColorMap',color_map);
    addParameter(p,'SaveTiff',tif);
    addParameter(p,'Name',name);
	addParameter(p,'ClassProbabilities',cprob);
    parse(p,varargin{:});
	
	if (p.Results.ClassProbabilities == false)
		%Anonymous function applied to block_proc_par
		fun = @(im) svmPredictMatrix(im, model);
		%Call of block_proc_par
		classMap = obj.block_proc_par(fun,'Bands',bands,'Limits',limits);	
	else
		%Anonymous function applied to block_proc_par
		fun = @(im) svmPredictMatrix(im, model,'ClassProbabilities', true);
		%Call of block_proc_par
		temp = obj.block_proc_par(fun,'Bands',bands,'Limits',limits);
		classMap.image = uint8(temp.image(:,:,1));
 		classMap.prob = temp.image(:,:,2:end);
		classMap.geoReference = temp.geoReference;
		classMap.geoInfo = temp.geoInfo;
	end
	
    %Save geotiff
    if(p.Results.SaveTiff)
		[a,b]=regexp(obj.filename,'S2[AB]');
        if ~exist('CM','dir') mkdir('CM'); end
        txt =['CM/' obj.filename(a:b) '_' datestr(obj.date_time,'yyyy-mm-dd') '_' obj.tile '_' p.Results.Name];
        if(isempty(p.Results.ColorMap))
            geotiffwrite([txt '.tif'],uint8(classMap.image), classMap.geoReference,'CoordRefSysCode', char(classMap.geoInfo.CoordSyst));
        else
            geotiffwrite([txt '.tif'],uint8(classMap.image), p.Results.ColorMap, classMap.geoReference,'CoordRefSysCode', char(classMap.geoInfo.CoordSyst));
        end
    end
end

