classdef S2data
	%RSDATA Summary of this class goes here
	%   Detailed explanation goes here 
% 	https://www.mathworks.com/company/newsletters/articles/introduction-to-object-oriented-programming-in-matlab.html
	
	properties
		path;
        filename;
        tile;
        level_processing;
        area;
        data;
        image;
		bands = {};
        %Limit of the normalization
        limit;
        scl;
        date_time;
        w_cut=[];
        geo_info;
        georeference_cut={};
        imageSize=[10980 10980];
        
	end
	
	methods        
		function obj=S2data(name,tile,varargin) 
            approxdate='closest';
            p=inputParser;
            addParameter(p,'ApproxDate',approxdate);
            parse(p,varargin{:});
            if and(and(~contains(name,'.SAFE'),~contains(name,'.zip')),~contains(name,'.mat')) 
                [a,b]= regexp(name,'\w*:.*?');
                if isempty(or(a,b))==1
                    [a,b]= regexp(name,'\d*-\d*-\d*');
                    if isempty(or(a,b))==1
                        name=['http://rslab-tech.disi.unitn.it/dataset/S2.php?file=' name];
                        data = jsondecode(webread(name));
                        name = data.file;
                    else
                        name=['http://rslab-tech.disi.unitn.it/dataset/S2.php?tile=' tile '&date=' name '&method=' p.Results.ApproxDate];
                        data = jsondecode(webread(name));
                        if isempty(data.file) msg='No data found with those parameters. '; error(msg); end
                        name=data.file;
                    end
                end
            end
%             [~,filename,ext] = fileparts(name);
            [~,filename,~] = fileparts(name);
            obj.filename=filename;
            [a, b] = regexp(name,'\d{8,}[T]\d'); 
            obj.date_time=datetime(name(a:b-2),'InputFormat','yyyyMMdd');
%             if nargin==2
            obj.tile=tile;
%             else obj.tile='UNKNOWN';
%             end
            
%             if strcmp(ext,'.mat')==0 && strcmp(ext,'.SAFE')==0 && strcmp(ext,'.zip')==0
%                 msg='ERROR with file extension. Expected a .mat or a folder .SAFE or a .zip';
%                 error(msg);
%             end
%             if isempty(strfind(name,'S2A')) && strcmp(ext,'.mat')==0
%                 msg='Not a S2A. ';
%                 error(msg);
%             end
            [a,b]=regexp(name,'L[0123][A-Z]');
%             if isempty(a) && strcmp(ext,'.mat')==0
%                 msg='Level of processing not detected in directory name. ';
%                 error(msg);
%             end
            obj.level_processing = name(a:b);
            obj.path = name;                
        end
        
        [ ] = geo_tiff_save( obj, varargin )
        obj = load_data( obj, varargin );
        obj = interpolation( obj, varargin );
        [out, obj] = linear_normalization(obj, I, varargin);
        [out, obj] = gamma_correction(obj,varargin);
        perc = percentile(obj, varargin);
        [ out ] = quick_look( obj, bands );
        lin_index = get_pixel( obj, x,y, b );
        [ w_cut ] = w_cut_finder( obj );
        [ out ] = bandmath( obj, funct, i );
        save_int(obj,path);
        [ TAB, label ] = shape_read(obj, name, field);
        out = block_proc_ser(obj, func ,varargin);
        out = block_proc_par( obj, func, varargin );
        [ classMap ] = svm_classification( obj, model, bands, limits, varargin  );
    end
    methods (Static = true, Access = private )
        [ path ] = search_file(obj, original_path, pattern, ext );
        geo_info = reads2GranuleXmlGeoInfo(xml_file, resolution);  
        georeference = read_geo_info(obj);
        [obj,temp,newdir] = unzip_file(obj);
        [ path ] = search_xml( obj,original_path );
        [] = clear_temp(temp);
    end
end

