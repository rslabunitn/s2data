function [geo_info] = reads2GranuleXmlGeoInfo(xml_file, resolution)
% Read the geografic matrix for the desired resolution 
%   xmFile      :   file to analyze
%   resolution  :   resolution (10,20 or 60)[m]
% Massimo Santoni June 2016

xdoc = xmlread(xml_file);

geo_info =  struct('ULX',[],'ULY',[],'XDIM',[],'YDIM',[],'NROWS',[],'NCOLS',[]);

allListitems = xdoc.getElementsByTagName('Geoposition');
for k = 0:allListitems.getLength-1
   thisListitem = allListitems.item(k);
   
   if (str2double(thisListitem.getAttribute('resolution')) == resolution)
        clist = thisListitem.getChildNodes;
      
        for j = 1:clist.getLength-1
          item = clist.item(j).get;
%           disp(item.NodeName);
          if(~strcmp('#text',item.NodeName))
              geo_info.(item.NodeName) = str2double(item.TextContent);
          end

        end
   end
end  
allListitems = xdoc.getElementsByTagName('Size');
for k = 0:allListitems.getLength-1
   thisListitem = allListitems.item(k);
   
   if (str2double(thisListitem.getAttribute('resolution')) == resolution)
        clist = thisListitem.getChildNodes;
      
        for j = 1:clist.getLength-1
          item = clist.item(j).get;
%           disp(item.NodeName);
          if(~strcmp('#text',item.NodeName))
              geo_info.(item.NodeName) = str2double(item.TextContent);
          end

        end
   end

end
geo_info.CoordSyst = xdoc.getElementsByTagName('HORIZONTAL_CS_CODE').item(0).getTextContent;
end
