function [ w_cut ] = w_cut_finder( obj ) 
%W_CUT_FINDER Summary of this function goes here
%   Detailed explanation goes here
    ulcut=pix2map(obj.georeference_cut,1,1);
    w_cut=[obj.geo_info.XDIM   0.0    ulcut(1); ...
           0.0   obj.geo_info.YDIM   ulcut(2)];
 
end