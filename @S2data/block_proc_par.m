function out = block_proc_par( obj, func, varargin )
%BLOCK_PROC Summary of this function goes here

%Default values and parser
    minForEachBand=[];
    maxForEachBand=[];
    if strcmp(obj.level_processing,'L2A')
        band={'01' '02' '03' '04' '05' '06' '07' '08' '8A' '09' '11' '12'};
    else
        if strcmp(obj.level_processing,'L1C')
            band={'01' '02' '03' '04' '05' '06' '07' '08' '8A' '09' '10' '11' '12'};
        end
    end
    p=inputParser;
    addParameter(p,'Bands',band);
    addParameter(p,'Limits',[minForEachBand maxForEachBand]);
	addParameter(p,'NOutBands',1);
    parse(p,varargin{:});

%Correction of Bands input
    for i=1:size(p.Results.Bands,2)
        [a, b] = regexp(p.Results.Bands{i},'\d[\d A]');
        if isempty(a)
            [a, b] = regexp(p.Results.Bands{i}, '\d');
            obj.bands{i}=['B0' p.Results.Bands{i}(a)];
        else
            obj.bands{i} =['B' p.Results.Bands{i}(a:b)];
        end
    end
    obj.bands=p.Results.Bands;
    
%Set limits if specified    
    if ~isempty(p.Results.Limits)
        obj.limit=[p.Results.Limits(:,1) p.Results.Limits(:,2)];
    end
    
%Unzip and blockproc if file is a .zip
    if contains(obj.path,'.zip')
        [obj,temp,newdir]=obj.unzip_file(obj);
        old_path=obj.path;
        obj.path=newdir;
        adapter=S2adapter(obj);
        newfunc=@(block_struct) func(adapter.readRegion(block_struct.location,block_struct.blockSize));
        out.image=blockproc(zeros([10980 10980 1]),[768 768],newfunc,'UseParallel',true);  
        georeference=obj.read_geo_info(obj);
        out.geoReference=georeference.R;
        out.geoInfo=georeference.geo_info;
%         obj.path=old_path;    
        obj.clear_temp(temp); 
    else
        
%Only blockproc if file is not a .zip
        adapter=S2adapter(obj);
        newfunc=@(block_struct) func(adapter.readRegion(block_struct.location,block_struct.blockSize));
        out.image=blockproc(zeros([10980 10980 1]),[768 768],newfunc,'UseParallel',true); 
        georeference=obj.read_geo_info(obj);
        out.geoReference=georeference.R;
        out.geoInfo=georeference.geo_info;
	end
	
	if(p.Results.NOutBands >1)
		size(out.image);
		
	end
end