function [] = geo_tiff_save( obj, varargin )
%GEO_TIFF_SAVE Summary of this function goes here
%   Detailed explanation goes here
    path=pwd;
    p=inputParser;
    addParameter(p,'path',path);
    parse(p,varargin{:});
    path=[p.Results.path '\geoTiff\'];
    if exist(path ,'dir')==0 
        mkdir(path);
    end
    [~, name, ~] = fileparts(obj.filename);
    geotiffwrite([path name '.tif'],obj.image, obj.georeference_cut, 'CoordRefSysCode', 'EPSG:32730'); 
 %   geotiffwrite([path name '.tif'],obj.image, obj.georeference_cut, 'CoordRefSysCode', char(obj.geo_info.CoordSyst));
end

