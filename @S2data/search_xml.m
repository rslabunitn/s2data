function [ path ] = search_xml( obj, original_path )
%SEARCH_XML Summary of this function goes here
%   Detailed explanation goes here
    path=[];
    list=dir(original_path);
    list=list(3:end);
    i=1;
    if isempty(list)
        return;
    end
    while(isempty(path))
        if and(and(contains(list(i).name,'.xml'),any(strcmp({list.name}, 'IMG_DATA'))),contains(original_path,obj.tile))
            path=[original_path '/' list(i).name];
        end
        if list(i).isdir
            path=obj.search_xml(obj,[original_path '/' list(i).name]);
        end
        i=i+1;
        if i>size(list,1) return;
        end
    end
end

