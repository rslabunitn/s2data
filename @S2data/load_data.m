function [ obj ] = load_data( obj, varargin )
%LOADDATA Summary of this function goes here
%   Detailed explanation goes here      
           % obj.data = importdata(obj.path);
 

    %% File .mat
    [~,~,ext]=fileparts(obj.path);
    if strcmp(ext,'.mat')==1
        obj.data = importdata(obj.path);
        return;
    end

    %% Default
    %band={'01' '02' '03' '04' '05' '06' '07' '08' '8A' '09' '10' '11' '12'};
    if strcmp(obj.level_processing,'L2A')
        band={'01' '02' '03' '04' '05' '06' '07' '08' '8A' '09' '11' '12'};
    else if strcmp(obj.level_processing,'L1C')
        band={'01' '02' '03' '04' '05' '06' '07' '08' '8A' '09' '10' '11' '12'};
        end
    end
    area={[1 10980],[1 10980]}; 
    
    %% Parser
    scl=0;
    temp=[tempname '/'];
    p=inputParser;
    addParameter(p,'Bands',band);
    addParameter(p,'Area',area);
    addParameter(p,'Scl',scl);
    addParameter(p,'Temp',temp);
    parse(p,varargin{:});
    
    %% Set bands
    for i=1:size(p.Results.Bands,2)
        [a, b] = regexp(p.Results.Bands{i},'\d[\d A]');
        if isempty(a)
            [a, b] = regexp(p.Results.Bands{i}, '\d');
            obj.bands{i}=['B0' p.Results.Bands{i}(a)];
        else
            obj.bands{i} =['B' p.Results.Bands{i}(a:b)];
        end
    end
        
    %% Set Area
%     area_cut=cell(size(p.Results.Bands,2),1);
%     for i=1:size(p.Results.Bands,2)
%         switch obj.bands{1,i}
%             case {'B01', 'B09', 'B10'}
%                 area_cut{i}={[fix(p.Results.Area{1,1}(1,1)/6)*6 ceil(p.Results.Area{1,1}(1,2)/6)*6],[fix(p.Results.Area{1,2}(1,1)/6)*6 ceil(p.Results.Area{1,2}(1,2)/6)*6]};
%             case {'B05', 'B06', 'B07', 'B8A', 'B11', 'B12'}
%                 area_cut{i}={[fix(p.Results.Area{1,1}(1,1)/2)*2 ceil(p.Results.Area{1,1}(1,2)/2)*2],[fix(p.Results.Area{1,2}(1,1)/2)*2 ceil(p.Results.Area{1,2}(1,2)/2)*2]};
%             case {'B02', 'B03', 'B04', 'B08'}
%                 area_cut{i}=p.Results.Area;
%             otherwise break;
%         end
%         if area_cut{i,1}{1,1}(1,1)==0
%             area_cut{i,1}{1,1}(1,1)=1;
%         end
%         if area_cut{i,1}{1,2}(1,1)==0
%             area_cut{i,1}{1,2}(1,1)=1;
%         end
%     end
   % area_round={[fix(p.Results.Area{1,1}(1,1)/6)*6 ceil(p.Results.Area{1,1}(1,2)/6)*6],[fix(p.Results.Area{1,2}(1,1)/6)*6 ceil(p.Results.Area{1,2}(1,2)/6)*6]};
   area_round={[p.Results.Area{1,1}(1,1)-mod((p.Results.Area{1,1}(1,1)-1),6), ...
                p.Results.Area{1,1}(1,2)+5-mod((p.Results.Area{1,1}(1,2)+5),6)], ...
                [p.Results.Area{1,2}(1,1)-mod((p.Results.Area{1,2}(1,1)-1),6), ...
                p.Results.Area{1,2}(1,2)+5-mod((p.Results.Area{1,2}(1,2)+5),6)]};
%     if area_round{1,1}(1,1)==0
%         area_round{1,1}(1,1)=1;
%     end
%     if area_round{1,2}(1,1)==0
%         area_round{1,2}(1,1)=1;
%     end
    area_cut=cell(size(p.Results.Bands,2),1);
    for i=1:size(p.Results.Bands,2)
        switch obj.bands{1,i}
            case {'B01', 'B09', 'B10'}
                %area_cut{i,1}={[area_round{1,1}(1,1)/6 area_round{1,1}(1,2)/6],[area_round{1,2}(1,1)/6 area_round{1,2}(1,2)/6]};
                area_cut{i,1}={[ceil(area_round{1,1}(1,1)/6) ceil(area_round{1,1}(1,2)/6)],[ceil(area_round{1,2}(1,1)/6) ceil(area_round{1,2}(1,2)/6)]};
                vec(i)={'60'};
            case {'B05', 'B06', 'B07', 'B8A', 'B11', 'B12'}
                %area_cut{i,1}={[area_round{1,1}(1,1)/2 area_round{1,1}(1,2)/2],[area_round{1,2}(1,1)/2 area_round{1,2}(1,2)/2]};
                area_cut{i,1}={[ceil(area_round{1,1}(1,1)/2) ceil(area_round{1,1}(1,2)/2)],[ceil(area_round{1,2}(1,1)/2) ceil(area_round{1,2}(1,2)/2)]};
                vec(i)={'20'};
            case {'B02', 'B03', 'B04', 'B08'}
                %area_cut{i,1}={[area_round{1,1}(1,1)/1 area_round{1,1}(1,2)/1],[area_round{1,2}(1,1)/1 area_round{1,2}(1,2)/1]};
                area_cut{i,1}={[ceil(area_round{1,1}(1,1)/1) ceil(area_round{1,1}(1,2)/1)],[ceil(area_round{1,2}(1,1)/1) ceil(area_round{1,2}(1,2)/1)]};
                if p.Results.Area{1,1}(1,2)>=10980
                    area_cut{i,1}{1,1}(1,2)=10980;
                end
                if p.Results.Area{1,2}(1,2)>=10980
                    area_cut{i,1}{1,2}(1,2)=10980;
                end
                vec(i)={'10'};
            otherwise, break;
        end
%         if area_cut{i,1}{1,1}(1,1)==0
%             area_cut{i,1}{1,1}(1,1)=1;
%         end
%         if area_cut{i,1}{1,2}(1,1)==0
%             area_cut{i,1}{1,2}(1,1)=1;
%         end
    end
     

    %% Unzip
    percorso = obj.path;
    if isempty(strfind(obj.path,'.zip'))==0
        pathtemp=unzip(percorso,p.Results.Temp);
        i=1;
        while ~contains(pathtemp{i},'GRANULE')
            i=i+1;
        end
        [a,~]=regexp(pathtemp{i},'[\\\/]GRANULE[\\\/]');
        percorso=pathtemp{i}(1:a);
    end
    
    %% Path finder      
    percorso=[percorso '/GRANULE/']; 
    x=dir(percorso);
    for i=3:size(x,1)
        str=x(i).name;
        if size(strfind(str,obj.tile),1)>=1
            break;
        end
    end
    georef_percorso=[percorso str '/'];  
    percorso=[percorso str '/IMG_DATA/']; 
    list={};
    if strcmp(obj.level_processing,'L2A')
            str=[percorso 'R10m'];
            list=(dir(fullfile(str, '*.jp2')));
            str=[percorso 'R20m'];
            list(size(list,1)+1:size(list,1)+size(dir(fullfile(str,'*.jp2'))))= dir(fullfile(str,'*.jp2'));
            str=[percorso 'R60m'];
            list(size(list,1)+1:size(list,1)+size(dir(fullfile(str,'*.jp2'))))= dir(fullfile(str,'*.jp2'));
    else if strcmp(obj.level_processing,'L1C')
        list = dir(fullfile(percorso,  '*.jp2'));
        end
    end
    
    %% Georefentiation
    list_geo = dir(fullfile(georef_percorso, '*.xml'));
    georef_percorso=[georef_percorso list_geo.name];
    geo_info =obj.reads2GranuleXmlGeoInfo(georef_percorso,10);
    W = [geo_info.XDIM   0.0   geo_info.ULX; ...
          0.0  geo_info.YDIM   geo_info.ULY];
    R = maprasterref(W,[geo_info.NROWS geo_info.NCOLS]);
    
    ulcut = pix2map(R, area_round{1,1}(1,1),area_round{1,2}(1,1));
    lrcut = pix2map(R, area_round{1,1}(1,2),area_round{1,2}(1,2));
    Wcut = [geo_info.XDIM   0.0   ulcut(1); ...
            0.0  geo_info.YDIM   ulcut(2)];
    Rcut = maprasterref(Wcut,[area_round{1,1}(1,2)-area_round{1,1}(1,1)+1,area_round{1,2}(1,2)-area_round{1,2}(1,1)+1]);
    obj.georeference_cut = Rcut;
    obj.geo_info = geo_info;
    
    
    %% Main
    
    x=size(obj.bands,2);
    data=cell(x,1);
    % out=cell(x,1);
    l=zeros(1,x);


    selected_band=cell(size(list,1),1);
    for i=1:x
        data{i}.pattern_name = obj.bands{1,i}; 
    end
    for j=1:x
        for i=1:size(list,1)
            if strfind(list(i).name,data{j}.pattern_name)>=1
                 l(j)=1;
                selected_band{i}.real_band=data{j}.pattern_name;
                selected_band{i}.area=area_cut{j};
                break;
            end
        end
    end
    if prod(l)==0
        msg = ('At least one band is missing. ');
        error(msg);
    end

    for i=1:size(selected_band,1)
        if ~isempty(selected_band{i})
            for j=1:size(data,1)
                if data{j}.pattern_name==selected_band{i}.real_band
                    %data{j}.name=selected_band{i}.real_band;
                    area_selected=selected_band{i}.area;
                    if strcmp(obj.level_processing,'L2A')
                    obj.data{j,1}=imread([percorso 'R' vec{j} 'm/' list(i).name],'PixelRegion',area_selected);
                    else if strcmp(obj.level_processing,'L1C')
                    obj.data{j,1}=imread([percorso list(i).name],'PixelRegion',area_selected);
                        end
                    end
                    obj.area{j,1}=area_selected;
                end
            end
        end
    end
        
    if p.Results.Scl==1
        for i=1:size(list,1)
            if strfind(list(i).name,'SCL')>= 1
                j=i;
                break;
            end
            if i==size(list,1)
                msg='SCL not found';
                error(msg);
            end
        end
        %scl = imread([percorso 'R20m/' list(j).name]);
        scl = imread([percorso 'R20m/' list(j).name],'PixelRegion',{[ceil(area_round{1,1}(1,1)/2) ceil(area_round{1,1}(1,2)/2)],[ceil(area_round{1,2}(1,1)/2) ceil(area_round{1,2}(1,2)/2)]});
        
        
        if p.Results.Area{1,1}(1,2) && p.Results.Area{1,2}(1,2) <=10976
            obj.scl = imresize(scl,[ceil(area_round{1,1}(1,2))-ceil(area_round{1,1}(1,1))+1 ceil(area_round{1,2}(1,2))-ceil(area_round{1,2}(1,1))+1],'nearest');
        end
        
        if and(p.Results.Area{1,1}(1,2)>10976, p.Results.Area{1,2}(1,2)>10976)
            obj.scl = imresize(scl,[ceil(area_round{1,1}(1,2)-1)-ceil(area_round{1,1}(1,1))+1 ceil(area_round{1,2}(1,2)-1)-ceil(area_round{1,2}(1,1))+1],'nearest');
        else
            if p.Results.Area{1,1}(1,2)>10976    
            obj.scl = imresize(scl,[ceil(area_round{1,1}(1,2)-1)-ceil(area_round{1,1}(1,1))+1 ceil(area_round{1,2}(1,2))-ceil(area_round{1,2}(1,1))+1],'nearest');
            end
            if p.Results.Area{1,2}(1,2)>10976 
            obj.scl = imresize(scl,[ceil(area_round{1,1}(1,2))-ceil(area_round{1,1}(1,1))+1 ceil(area_round{1,2}(1,2)-1)-ceil(area_round{1,2}(1,1))+1],'nearest');
            end
        end
        %scl = imresize(scl,[10980 10980]);
        %obj.scl=scl(area_round{1,1}(1,1):area_round{1,1}(1,2),area_round{1,2}(1,1):area_round{1,2}(1,2));
    end

    if isempty(strfind(obj.path,'.zip'))==0
        rmdir(p.Results.Temp, 's'); 
    end
    
end

