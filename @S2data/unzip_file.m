function  [obj,temp,newdir] = unzip_file(obj)
%UNZIP_FILE Summary of this function goes here
%   Detailed explanation goes here
    percorso = obj.path;
    temp = [tempname '/'];
    if isempty(strfind(percorso,'.zip'))==0
        pathtemp=unzip(percorso,temp);
        i=1;
        while ~contains(pathtemp{i},'GRANULE')
            i=i+1;
        end
        [a,b]=regexp(pathtemp{i},'[\\\/]GRANULE[\\\/]');
        newdir=pathtemp{i}(1:a);
    end
    
    geodir=dir(pathtemp{i}(1:b));
    for i=3:size(geodir,1)
        str=geodir(i).name;
        if size(strfind(str,obj.tile),1)>=1
            break;
        end
    end
    georef_percorso=[newdir 'GRANULE/' str '/'];
    list_geo = dir(fullfile(georef_percorso, '*.xml'));
    georef_percorso=[georef_percorso list_geo.name];
    geo_info =obj.reads2GranuleXmlGeoInfo(georef_percorso,10);
    W = [geo_info.XDIM   0.0   geo_info.ULX; ...
          0.0  geo_info.YDIM   geo_info.ULY];
    R = maprasterref(W,[geo_info.NROWS geo_info.NCOLS]);
    obj.georeference_cut = R;
    obj.geo_info = geo_info;
    
end

