function [ out, obj ] = gamma_correction( obj,varargin )
%GAMMA_CORRECTION Summary of this function goes here
%   Detailed explanation goes here
    I=[];
    p = inputParser;
	addParameter(p,'Gamma',1);
    addParameter(p,'load_image',0);
    addParameter(p,'image',I);
 	parse(p,varargin{:});
% 	disp(obj.filename);
%	disp(['Gamma:' num2str(p.Results.Gamma)]);
    if ~isempty(p.Results.image)
        out=p.Results.image.^p.Results.Gamma;
    return;
    end

    if p.Results.load_image==1
        obj.image=obj.image.^p.Results.Gamma;
    end
    out=obj.image.^p.Results.Gamma;
        
    
    
end

