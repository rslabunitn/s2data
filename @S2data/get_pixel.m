
function [ lin_index ] = get_pixel( obj, x,y, b )
%MAP2PIX Summary of this function goes here
%   Detailed explanation goes here
    del = x<obj.georeference_cut.XWorldLimits(1,1) ...
        | x>obj.georeference_cut.XWorldLimits(1,2) ...
        | y<obj.georeference_cut.YWorldLimits(1,1) ...
        | y>obj.georeference_cut.YWorldLimits(1,2);
    x(del)=[];
    y(del)=[];
    
%     A=combvec(x,y);
%     [row,col] = map2pix(obj.georeference_cut,A(1,:),A(2,:));
    [row,col] = map2pix(obj.georeference_cut,x,y);
    row=round(row);
    col=round(col);
%     lin_index=sub2ind(size(obj.image),row, col, b*ones(size(row)));
    lin_index=sub2ind(size(obj.image),row, col, b*ones(size(x)));
end


