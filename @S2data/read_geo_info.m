function georeference = read_geo_info(obj)
%READ_GEO_INFO Summary of this function goes here
%   Detailed explanation goes here
    path = obj.search_xml(obj,obj.path);      %['*' obj.tile '*xml']);
    georeference.geo_info =obj.reads2GranuleXmlGeoInfo(path,10);
    georeference.W = [georeference.geo_info.XDIM   0.0   georeference.geo_info.ULX; ...
                      0.0  georeference.geo_info.YDIM   georeference.geo_info.ULY];
    georeference.R = maprasterref(georeference.W,[georeference.geo_info.NROWS georeference.geo_info.NCOLS]);
end

