function [ result ] = svmPredictMatrix( image, model, varargin )
%SVMPREDICTMATRIX libsvmpredicT on  matrix data

p=inputParser;
addParameter(p,'ClassProbabilities',false);
addParameter(p,'MaxMin',false);
parse(p,varargin{:});

if p.Results.MaxMin
    for i=1:size(maxmin,2)
        maxval = p.Results.MaxMin(1,i);
        minval = p.Results.MaxMin(2,i);
        image(:,:,i) = ((image(:,:,i))-minval) ./(maxval-minval);
    end
    image(image<0) = 0;
    image(image>1) = 1;
end

samples=reshape(image,[size(image,1)*size(image,2),size(image,3)]); 

if(p.Results.ClassProbabilities == false)
	[predictLabel,~, ~]=svmpredict(double(zeros(size(samples,1),1)),double(samples), model);
	result=reshape(predictLabel,[size(image,1), size(image,2)]); 
else
	[predictLabel,~, predictProb]=svmpredict(double(zeros(size(samples,1),1)),double(samples), model,'-b 1');
	classProb = reshape(predictProb,[size(image,1), size(image,2), model.nr_class]);
	cmap=reshape(predictLabel,[size(image,1), size(image,2)]); 
	result = cat(3, cmap, classProb);
end


end

