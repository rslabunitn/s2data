classdef S2adapter < ImageAdapter
   properties(Access = public)
      S2data=[];
   end
   methods
%% Constructor
       function obj=S2adapter(s2)
           obj.S2data=s2;
           obj.ImageSize=[10980 10980];
       end
%% Read Region       
       function [ data ] = readRegion( obj, region_start, region_size )
            obj.S2data=obj.S2data.load_data('Bands',obj.S2data.bands,'Area',{[region_start(:,1) region_start(:,1)+region_size(:,1)-1], ...
                                                                            [region_start(:,2) region_start(:,2)+region_size(:,2)-1]});
            obj.S2data=obj.S2data.interpolation('Method','nearest');
            if ~isempty(obj.S2data.limit)
                [~,obj.S2data]=obj.S2data.linear_normalization(obj.S2data.image,'load_image',1,'Limits',obj.S2data.limit);
            end
            data=obj.S2data.image;
       end
%% Close
    function close(obj) %#ok<MANU>
          % Close the LanAdapter object. This method is a part
          % of the ImageAdapter interface and is required.
          % Since the readRegion method is "atomic", there are
          % no open file handles to close, so this method is empty.
    end
   end
end