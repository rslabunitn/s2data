
addpath(genpath('lib'));

%load training set, define SVM parameters and normalization ranges of the bands used
trainSet=double(importdata('trainSet_noAcqua.mat'));
cmd='-t 2 -c 469.3793 -g 193.4483';
svmModel.Limits = [699 19572;441 18430;268 21170;169 21144;10 15073;0 14058];
bands={'2' '3' '4' '8' '11' '12'};

minVal = svmModel.Limits(:,1);
maxVal = svmModel.Limits(:,2);

% normalize the training set
for i=2:size(trainSet,2)
	trainSet(:,i) = (trainSet(:,i) - minVal(i-1))/(maxVal(i-1) - minVal(i-1));
end

% Train the SVM
svmModel.model = svmtrain(trainSet(:,1),trainSet(:,2:end), [cmd, ' -b 1'] );  \

%%

tile = '32TPS';

poolobj = parpool('local',11);

% S2Snow(file, tile, svmModel);

file = 'E:\s2\S2A_OPER_PRD_MSIL1C_PDMC_20160522T102029455Z_R065_T32TPS.SAFE';
S2Snow(file, tile, svmModel, bands);

% dateList = [datenum(2017,02,16);...
% 			datenum(2017,02,23);...
% 			datenum(2017,05,04)];
% 
% for j=1:numel(dateList)
% 	i=dateList(j);
% 	dayStr =  datestr(i, 'yyyy-mm-dd');
% 	disp(dayStr);
% 	try
% 		S2Snow(dayStr, tile, svmModel);
% 	catch err
% 		disp(['###---' dayStr]);
% 		disp(['error ' err.message]);
% 	end
% end

delete(poolobj);

disp('# # # # END # # # #');