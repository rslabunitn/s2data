function S2Snow(name, tile, svmModel, bands)
% Snow classification using Tomamso Scuccato model and soave Weikmann object
% if the model suppports class probabilites they will computed and saved to geotiff (single).
% 
%  require libsvm mex files in 'lib' folder and svmPRedictMatrix function
%  Massimo Santoni 9-2017

addpath(genpath('lib'));

s=S2data(name,tile); %,'ApproxDate','exact'
nameOut = 'snowMaps';
nameOutProb = 'snowMapsProb';

outFolder = 'CM/';

if nargin<4
	bands={'2' '3' '4' '8' '11' '12'};
end

colorMap=[255 255 255; 155 202 199; 0 255 0; 0 121 0; 129 129 129; 0 0 100]./255; 

if(numel(svmModel.model.ProbA) > 0)
	ClassProbabilities = true;
else
	ClassProbabilities = false;
end

classMap = s.svm_classification(svmModel.model, ...
			bands, ...
			svmModel.Limits,...
			'ClassProbabilities', ClassProbabilities,...
			'SaveTiff',false,...
			'ColorMap',colorMap,...
			'Name', 'snowMap');

		
[a,b]=regexp(s.filename,'S2[AB]');

if ~exist('CM','dir') 
	mkdir('CM');
end

txt =[outFolder s.filename(a:b) '_' datestr(s.date_time,'yyyy-mm-dd') '_' s.tile '_' nameOut];
geotiffwrite([txt '.tif'],uint8(classMap.image), [0 0 0; colorMap], classMap.geoReference,'CoordRefSysCode', char(classMap.geoInfo.CoordSyst));

if (ClassProbabilities)
% if a uint16 geotiff is needed:
% 	prob = uint16(classMap.prob * ((2^16)-1));
	prob = single(classMap.prob);
	txt =[outFolder s.filename(a:b) '_' datestr(s.date_time,'yyyy-mm-dd') '_' s.tile '_' nameOutProb];
	geotiffwrite([txt '.tif'],prob, classMap.geoReference,'CoordRefSysCode', char(classMap.geoInfo.CoordSyst));
end

end